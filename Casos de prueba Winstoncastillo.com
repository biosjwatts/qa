Autor: Juan Carlos Watts
Título: W001 ir al blog de Winston Castillo
Objetivo: Probar que el link se mantiene funcionando.
Tipo de caso de prueba: positiva
Tipo de prueba: funcionalidad.

PAsos de la prueba
1- IR a www.winstoncastillo.com/udemy
2- Cargar la ventana con el título "Hola Mundo" hacer click en "Este link te direje a otra ventana"

Resultado esperado
El click debe dirigir a una página cuya dirección es http://blog.winstoncastillo.com

Autor: Juan Carlos Watts
Título: W002 abrir ventana modal
Objetivo: verificar que la ventana modal esté funcionando.
Tipo de caso de prueba: positiva
Tipo de prueba: funcionalidad.

PAsos de la prueba
1- IR a www.winstoncastillo.com/udemy
2- Cargar la ventana con el título "Hola Mundo" hacer click en "Este link hace aparecer una ventana nueva"

Resultado esperado
El click debe mostrar un modal y te debe mantener en la misma página.


Test Suite

